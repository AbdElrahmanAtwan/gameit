using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class BallSpawner : MonoBehaviour
{
    public int CurrentTurn=1;

    [SerializeField]
    float Range = 10;
    [SerializeField]
    GameObject BallType;
    bool SpawnDone = false;
    [SerializeField]
    Material[] DefaultMats;
    [SerializeField]
    Image[] Images;

    // Start is called before the first frame update
    void Start()
    {

    }
	// Update is called once per frame
	void Update()
    {
        
    }
    public void SpawnBalls()
    {
        Material[] tmp = DefaultMats;
        if (!SpawnDone)
		{
            for (int i = 1; i <= DefaultMats.Length; i++)
            {
                GameObject obj = Instantiate(BallType, GetRandomLocation(), Quaternion.identity);
                obj.GetComponent<BallScript>().BallSpawnerRef = this;
                obj.GetComponent<BallScript>().BallTurn = i;
                int RandomColor = Random.Range(0, tmp.Length-1);
                obj.GetComponent<BallScript>().ChangeMaterial(tmp[RandomColor]);
                Images[i - 1].color = tmp[RandomColor].color;
                RemoveAt(ref tmp, RandomColor);
            }
            SpawnDone = true;
        }
    }
    void RemoveAt<T>(ref T[] arr, int index)
    {
        arr[index] = arr[arr.Length - 1];
        Array.Resize(ref arr, arr.Length - 1);
    }
    private Vector3 GetRandomLocation()
    {
        Vector3 NewLocation = transform.position;
        NewLocation.x = NewLocation.x + Random.Range(-Range, Range);
        NewLocation.z = NewLocation.z + Random.Range(-Range, Range);
        return NewLocation;
    }
}
